let movies;

describe("Registration use case", () => {
    beforeEach(() => {
        cy.visit("/register");
      });
  describe("Navigate to registration page", () => {
    it("Navigates to registration page", () => {    
        cy.get("button").contains("Register").click();  
        cy.url().should("include", `/register`);
    });
    });
    describe("Register account", () => {
    it("Enter account details & Check if user logged in, then logout", () => {    
        const accountName = "Evan1234";
        const email = "evancasey1234@gmail.com";
        const password = "1234567";

        cy.get("button").contains("Register").click();  
        cy.url().should("include", `/register`);
        
        cy.get("#registration-name").clear().type(accountName);   
        cy.get("#registration-email").clear().type(email); 
        cy.get("#registration-password").clear().type(password); 

        cy.get("#register_movie_app_button").contains("Register").click();

        cy.get("#user_display_email").contains(email);
        cy.wait(500)
        cy.get("button").contains("Logout").click();  
    });
    });
});
