import React, { useContext } from "react";
import { useLocation, Navigate } from "react-router-dom";
import PageTemplate from "../components/templateMoviePage";
import MovieReview from "../components/movieReview";
import AuthContext from "../AuthContext";

const MovieReviewPage = (props) => {
  let location = useLocation();
  const { movie, review } = location.state;

  const { user } = useContext(AuthContext);
  if (!user) {
    return <Navigate replace to="/login" />;
  }
  return (
    <PageTemplate movie={movie}>
      <MovieReview review={review} />
    </PageTemplate>
  );
};

export default MovieReviewPage;